<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Title</title>
    <script src="http://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
</head>
<body>

<?php
include './../src/autoloader.php';
$sRjgcWechat=new \SRjgcWechat\SRjgcWechat();
$jssdk=$sRjgcWechat->jssdk();
$data=$jssdk->shareWx();

?>
<script>
    wx.config({
        debug: true, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
        appId: 'wx5535c4e87f38d494', // 必填，公众号的唯一标识
        timestamp:<?php echo $data['timestamp']?> , // 必填，生成签名的时间戳
        nonceStr: '<?php echo $data['nonceStr']?>', // 必填，生成签名的随机串
        signature: '<?php echo $data['signature']?>',// 必填，签名
        jsApiList: [
            'onMenuShareTimeline',
            'onMenuShareAppMessage',
            'chooseImage',
            'scanQRCode'
        ] // 必填，需要使用的JS接口列表
    });

    wx.ready(function(){
        // config信息验证后会执行ready方法，所有接口调用都必须在config接口获得结果之后，config是一个客户端的异步操作，所以如果需要在页面加载时就调用相关接口，则须把相关接口放在ready函数中调用来确保正确执行。对于用户触发时才调用的接口，则可以直接调用，不需要放在ready函数中。

        //分享到朋友圈
        wx.onMenuShareTimeline({
            title: '分享标题-分享到朋友圈', // 分享标题
            link: 'http://www.hao123.com', // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
            imgUrl: 'https://www.baidu.com/img/bdlogo.png', // 分享图标
            success: function () {
                // 用户确认分享后执行的回调函数
                alert('分享成功');
            },
            cancel: function () {
                // 用户取消分享后执行的回调函数
                alert('请重新分享');
            }
        });

        //分享到朋友
        wx.onMenuShareAppMessage({
            title: '分享标题-分享到朋友', // 分享标题
            desc: '分享标题-分享到朋友-描述', // 分享描述
            link: 'http://www.hao123.com', // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
            imgUrl: 'https://www.baidu.com/img/bdlogo.png', // 分享图标
            type: 'link', // 分享类型,music、video或link，不填默认为link
            dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
            success: function () {
                // 用户确认分享后执行的回调函数
                alert('分享成功');
            },
            cancel: function () {
                // 用户取消分享后执行的回调函数
                alert('请重新分享');
            }
        });


    });

    selChooseImage(){
        //拍照
        wx.chooseImage({
            count: 1, // 默认9
            sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
            sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
            success: function (res) {
                var localIds = res.localIds; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
            }
        });
    }

    function scan() {
        wx.scanQRCode({
            needResult: 0, // 默认为0，扫描结果由微信处理，1则直接返回扫描结果，
            scanType: ["qrCode","barCode"], // 可以指定扫二维码还是一维码，默认二者都有
            success: function (res) {
                var result = res.resultStr; // 当needResult 为 1 时，扫码返回的结果
            }
        });
    }




    wx.error(function(res){
        // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
    });

</script>
<button class="selChooseImage();">调取相册</button>
<button class="scan();">扫一扫</button>
</body>
</html>